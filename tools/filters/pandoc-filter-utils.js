#!/bin/node

var pandoc = require('pandoc-filter');

module.exports.get_lang = function( el ) {
    if (el.t === 'Div' || el.t === 'Span') {
        var el = el.c[0][2].find( function (attr) {
            return attr[0] === 'lang';
        });
        
        if (typeof el !== 'undefined') return el[1];
    }
}

module.exports.is_in_class = function( cls, attr ) {
  return attr[1].indexOf( cls ) >= 0;
};

module.exports.add_class = function( cls, attrs ) {
  var i = attrs[1].indexOf( cls );
  if ( i < 0 ) {
      attrs[1].push( cls );
  }

  return attrs;
};

module.exports.remove_class = function( cls, attrs ) {
  var i = attrs[1].indexOf( cls );
  if ( i >= 0 ) {
      attrs[1].splice( i, 1 );
  }

  return attrs;
};

module.exports.modify_class = function( prevCls, curCls, attrs ) {
  var i = attrs[1].indexOf( prevCls );
  if ( i >= 0 ) {
    attrs[1].splice( i, 1, curCls );
  }

  return attrs;
};

module.exports.wrap_with_tex_code = function( values, before, after ) {
  values.unshift( pandoc.RawBlock( 'tex', before ) );
  values.push( pandoc.RawBlock( 'tex', after ) );
  return values;
};

module.exports.inline_wrap_with_tex_code = function( values, before, after ) {
  values.unshift( pandoc.RawInline( 'tex', before ) );
  values.push( pandoc.RawInline( 'tex', after ) );
  return values;
};
