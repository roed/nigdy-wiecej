#!/bin/node

const 
    default_language = 'pl',
    default_format = 'default',

    get_lang_from_metadata = function(meta) {
        if (meta.hasOwnProperty("lang")) {
            return meta.lang.c[0].c;
        }  else {
            return default_language;
        }
    },
    
    get_item_lang = function( el ) {
        if (el.t === 'Div' || el.t === 'Span') {
            var el = el.c[0][2].find( function (attr) {
                return attr[0] === 'lang';
            });
            
            if (typeof el !== 'undefined') return el[1];
        }
    },

    append_to_path = function( info, v  ) {
        if( v === 'c' ) {
            return info;
        } else {
            return { path: info.path.concat( [v] ), lang: info.lang };
        }
    },

    walk = function(x, action, format, meta, info) {
        const walker = function(x,info) {
            if (Array.isArray(x)) {
                const array = [];
                const array_push = function(z) { array.push(z); };
                
		for(const item of x) {
                    if (typeof item === 'object' && item.t) {
                        let cinfo = info;
                        const ninfo = append_to_path( info, item.t );
                        const innerLang = get_item_lang(item);
                        
                        if (typeof innerLang !== 'undefined') {
                            cinfo = { path: info.path, lang: innerLang };
                            ninfo.lang = innerLang;
                        }

                        const res = action(item.t, item.c, format, meta, cinfo);
                        if (!res) {
                            array_push(walker(item, ninfo));
                        } else if (Array.isArray(res)) {
			    for(const y of res) {
				walker([ y ], ninfo).forEach(array_push);
                            };
                        } else {
                            array_push(walker(res, ninfo));
                        }
                    } else {
                        array_push(walker(item, info));
                    }
                };
                
                return array;
            }
            else if (typeof x === 'object' && x !== null ) {
                const obj = {};
                
		for(const k in x) {
                    obj[k] = walker(x[k], append_to_path( info, k ));
                };
                
                return obj;
            }
            
            return x;
        };
        
        return walker(x,info);
    },
    
    filter = function(data, action, format) {
        return {
            blocks: walk(data.blocks, action, format, data.meta, {path: [], lang: get_lang_from_metadata(data.meta)} ),
            "pandoc-api-version": data["pandoc-api-version"],
            meta: data.meta
        };
    },
    
    toJSONFilter = function(action) {
        require('get-stdin')(function (json) {
            var data = JSON.parse(json);
            var format = (process.argv.length > 2 ? process.argv[2] : '');
            var output;
            if ( typeof action === 'function') {
                output = filter(data, action, format);
            } else if ( action.hasOwnProperty(format) ) {
                output = filter(data, action[format], format);
            } else if ( action.hasOwnProperty(default_format) ) {
                output = filter(data, action[default_format], format);
            } else {
                console.error('Usupported format: ' + format);
                output = data;
            }
            process.stdout.write(JSON.stringify(output));
        });
    };


module.exports = {
    toJSONFilter: toJSONFilter,
    stdio: toJSONFilter,
    walk: walk,
    filter: filter,
    defaultActionName: default_format,
    defaultLanguage: default_language
};
