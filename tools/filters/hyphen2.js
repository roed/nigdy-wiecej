#!/bin/node

const
    pandocEx = require('./pandoc-filter'),
    pandoc = require('pandoc-filter'),
    hyphen = require('hyphen'),
    
    hlangs = ["en-us","pl","it","la","fr","cs","grc"],
    
    ld_hyphers = function(langs) {
        const h = {};

	for(const lang of langs) {
            let mlang = lang;
            let p = lang.indexOf('-');
            if (p >= 0)  mlang = lang.slice(0,p);
            let l = require("hyphen/patterns/"+lang);
            let hp = hyphen(l, {debug: false});
            h[mlang] = hp;
        };

        return h;
    },
    
    h = ld_hyphers( hlangs ),

    action = function(type,value,format,meta,info) {
        if (type === 'Str') {
            if(info.path.indexOf('Header') < 0){
                if (h[info.lang]) {
                    return pandoc.Str( h[info.lang]( value ) );
                }
            }
        }
    },
    
    actions = { epub3: action, html: action };

pandocEx.stdio(actions);
