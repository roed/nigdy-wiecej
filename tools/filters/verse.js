#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    Para = pandoc.Para,
    Div = pandoc.Div,
    BlockQuote = pandoc.BlockQuote,
    utils = require('./pandoc-filter-utils'),

    split_paragraph = function(para) {
        const spl = [[]];
        // console.error( para );
        for(const e of para.c) {
            if (e.t === 'LineBreak') {
                spl.push( [] );
            } else {
                spl[ spl.length -1 ].push( e );
            }
        };

        const paragraphs = [];
        spl.forEach(function (e) {
            paragraphs.push( Para( e ) );
        });

        return Div( ['',['versep'],[]], paragraphs );
    },

    split_line_block = function(lb) {
        const paragraphs = [];
	for(const e of lb.c) {
            paragraphs.push( Para( e ) );
        };

        return Div( ['',['versep'],[]], paragraphs );
    },

    process_div_elements = function( values ) {
        const res = [];
	for(const e of values) {
            if ( e.t === 'Para' || e.t == 'Plain' ) {
                res.push( split_paragraph( e ) );
            }
            else if ( e.t === 'LineBlock' ) {
                res.push( split_line_block( e ) );
            }
            else {
                res.push( e );
            }
        };
        return res;
    },

    action_html = function(type,value,format,meta) {
        if (utils.is_in_class( 'verse', value[0] ) ) {
            const vers = process_div_elements( value[1] );
            return Div( value[0], vers );
        } else if (utils.is_in_class( 'versei', value[0] ) ) {
            return Div( utils.modify_class( 'versei', 'verse', value[0] ),
                [ BlockQuote( process_div_elements( value[1] ) ) ] );
        }
    },

    action_latex = function(type,value,format,meta) {
        if (utils.is_in_class( 'verse', value[0] ) ) {
            const vers = utils.wrap_with_tex_code( value[1],
                '\\setlength{\\saveleftmargini}{\\leftmargini} \\setlength{\\leftmargini}{0em} \\begin{verse}',
                '\\end{verse} \\setlength{\\leftmargini}{\\saveleftmargini}' );
            return Div( utils.remove_class( 'verse', value[0] ), vers );
        } else if (utils.is_in_class( 'versei', value[0] ) ) {
            return Div( utils.remove_class( 'versei', value[0] ),
                utils.wrap_with_tex_code( value[1], '\\begin{verse}', '\\end{verse}' )
            );
        }
    },
    
    get_div_action = function(action) {
        return function(type,value,format,meta,info) {
            if (type === 'Div') {
                return action(type,value,format,meta,info);
            }
        }
    },
    
    actions = { epub3: get_div_action(action_html), html: get_div_action(action_html), latex: get_div_action(action_latex) };

pandocEx.stdio(actions);
