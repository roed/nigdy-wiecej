#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    utils = require('./pandoc-filter-utils'),

    nbsp = String.fromCharCode(160),
    Space = pandoc.Space(),
    Str = pandoc.Str,

    action = function(type,value,format,meta) {
        if ( type === 'Str'  ) {
            var spl = value.split( nbsp );
            if (spl.length > 1) {
                var res = [];
                for(const v of spl) {
                    if (v.length > 0) {
                        res.push( Str( v ) );
                    }
                    res.push( Space );
                };
                res.pop(); // last extra space

                return utils.inline_wrap_with_tex_code( res, '\\mbox{', '}')
            }
        }
    }
    
    actions = { latex: action };

pandocEx.stdio(actions);
