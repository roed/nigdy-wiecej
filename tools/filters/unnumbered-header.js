#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    utils = require('./pandoc-filter-utils'),

    action_latex = function(type,value,format,meta) {
        // Header: [number, Attr, Array<Inline>];
        if (type === 'Header' && value[0] == 1 && utils.is_in_class( 'unnumbered', value[1] ) && !utils.is_in_class('nochaptermark', value[1]) ) {
            const res = [
                pandoc.Header(value[0], utils.add_class('nochaptermark', value[1]), value[2]),
                pandoc.RawBlock('tex', '\\chaptermark{' + pandoc.stringify(value[2]) + '}' )
            ];
            return res;
        }
    },
    
    actions = { latex: action_latex };

pandocEx.stdio(actions);
