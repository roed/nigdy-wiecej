#!/bin/node

const 
      pandoc = require('pandoc-filter'),
      Str = pandoc.Str,
      pandocEx = require('./pandoc-filter'),
      qmarks = { 
           pl:  [ "\u201E", "\u00BB", "\u00AB", "\u201D"],
           en:  [ "\u201C", "\u2018", "\u2019", "\u201D"],
           cs:  [ "\u201E", "\u2018", "\u2019", "\u201D"],
           fr: [ "\u00AB\u2005", "\u2039\u2005", "\u2005\u203A", "\u2005\u00BB"],
           de:  [ "\u201E", "\u201A", "\u2018", "\u201C"], 
           grc:  [ "\u00AB", "\u201F", "\u201D", "\u00BB"], 
           la: [ "\u00AB", "\u2039", "\u203A", "\u00BB"],
       },
       
      hardquote_fn = function( openingChars, closingChars ) {
        return function( v ) {
            if (v.length == 0) {
                return [ Str(openingChars.concat(closingChars)) ];
            } else {
                if (v[0].t === 'Str') {
                    v[0].c = openingChars.concat( v[0].c );
                } else {
                    v.unshift(Str(openingChars));
                }

                if (v[v.length-1].t === 'Str') {
                    v[v.length-1].c = v[v.length-1].c.concat(closingChars);
                } else {
                    v.push(Str(closingChars));
                }
                return v;
            }
        };
    },
    
    create_quoters = function(ldesc) {
        const actions = {};
        for(const lang in ldesc) {
            actions[lang] = {
                    double: hardquote_fn(ldesc[lang][0], ldesc[lang][3]),
                    single: hardquote_fn(ldesc[lang][1], ldesc[lang][2])
            };
        };
        
        return actions;
    },
    
    quoters = create_quoters(qmarks),
    
    unquote = function( q, v, lquoters ) {
        if (q.t === 'DoubleQuote' ) {
            return lquoters.double(v);
        } else if (q.t === 'SingleQuote') {
            return lquoters.single(v);
        }
    },
    
    action = function(type,value,format,meta,info) {
        if (type === 'Quoted') {
            if (quoters[info.lang]) {
                const lquoters = quoters[info.lang];
                return unquote(value[0],value[1],lquoters);
            }
        }
    },
    
    actions = { epub3: action, html: action };

pandocEx.stdio(actions);
