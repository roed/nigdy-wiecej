#!/bin/node

/*

    % The commands to select semibold weight:
    \DeclareRobustCommand\sbseries{\fontseries{sb}\selectfont}
    \DeclareTextFontCommand{\textsb}{\sbseries}

    % The command to select light weight
    \DeclareRobustCommand\ltseries{\fontseries{l}\selectfont}
    \DeclareTextFontCommand{\textlt}{\ltseries}

    % The command to select extra light weight
    \DeclareRobustCommand\elseries{\fontseries{el}\selectfont}
    \DeclareTextFontCommand{\textel}{\elseries}

*/

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    utils = require('./pandoc-filter-utils'),
    fontseries = [ 'sb', 'lt', 'el' ], // sb - SemiBold, lt - Light, el - ExtraLight

    fs_div = function(fs, attr, val) {
        return pandoc.Div(
                utils.remove_class( 'textsb', attr ),
                utils.wrap_with_tex_code( val, '{\\' + fs + 'series ', '}' )
            );
    },

    color_div = function(fs, attr, val) {
        return pandoc.Div(
                utils.remove_class( 'textsb', attr ),
                utils.wrap_with_tex_code( val, '{\\color{ctext' + fs + '}  ', '}' )
            );
    },

    fs_span = function(fs, attr, val) {
        return pandoc.Span(
                utils.remove_class( 'textsb', attr ),
                utils.inline_wrap_with_tex_code( val, '\\text' + fs + '{', '}' )
            );
    },

    color_span = function(fs, attr, val) {
        return pandoc.Span(
                utils.remove_class( 'textsb', attr ),
                utils.inline_wrap_with_tex_code( val, '\\textcolor{ctext' + fs + '}{', '}' )
            );
    },

    fs_search_fontseries_class = function(isDiv, attr, val ) {
      for(const fs of fontseries) {
          const cls = 'text' + fs;
          if (attr[1].indexOf( cls ) >= 0) {
               return isDiv? fs_div(fs, attr, val) : fs_span(fs, attr, val);
          }
      }
    },

    color_search_fontseries_class = function(isDiv, attr, val ) {
      for(const fs of fontseries) {
          const cls = 'text' + fs;
          if (attr[1].indexOf( cls ) >= 0) {
               return isDiv? color_div(fs, attr, val) : color_span(fs, attr, val);
          }
      }
    },

    action_latex = function(type,value,format,meta) {
        if (type == 'Div' || type === 'Span') {
            let fontseries = false;
            const metaFs=meta['fontseries'];
            if (metaFs && metaFs.t == 'MetaBool' && metaFs.c) {
                fontseries = true;
            }

            if (fontseries) {
                return fs_search_fontseries_class(type === 'Div', value[0], value[1]);
            } else {
                return color_search_fontseries_class(type === 'Div', value[0], value[1]);
            }
        }
    },
    
    actions = { latex: action_latex };

pandocEx.stdio(actions);
