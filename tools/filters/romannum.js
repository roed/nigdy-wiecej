#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    utils = require('./pandoc-filter-utils'),

    rnum_val = function(attrs) {
        var el = attrs.find( function (attr) {
            return attr[0] === 'rnum';
        })

        if (typeof el !== 'undefined') {
            return el[1];
        }
    },

    action_latex = function(type,value,format,meta) {
        if (type === 'Span') {
            if (utils.is_in_class( 'Romannum', value[0] ) ) {
                var rnum = rnum_val(value[0][2]);
                if (rnum) {
                    var tex = '\\uppercase\\expandafter{\\romannumeral'.concat(rnum,'\\relax}');
                    return pandoc.Span( value[0],
                        [ pandoc.RawInline('tex', tex )  ] );
                }
            }
            else if (utils.is_in_class( 'romannum', value[0] ) ) {
                var rnum = rnum_val(value[0][2]);
                if (rnum) {
                    var tex = '\\romannumeral'.concat(rnum,'\\relax');
                    return pandoc.Span( value[0],
                        [ pandoc.RawInline('tex', tex )  ] );
                }
            }
        }
    },
    
    actions = { latex: action_latex };

pandocEx.stdio(actions);
