#!/bin/node

const
    pandoc = require('pandoc-filter'),
    utils = require('./pandoc-filter-utils'),
    qr = require('qr-image'),
    xmldom = require('xmldom'),

    make_qr_svg = function( url ) {
        const
          rawSvg = qr.imageSync( url, { type: 'svg', ec_level: 'L'} ),
          domParser = new xmldom.DOMParser(),
          svg = domParser.parseFromString( rawSvg, 'text/xml' ),
          xmlSerializer = new xmldom.XMLSerializer();
        svg.documentElement.setAttribute('class', 'qrcode');
        svg.documentElement.setAttribute('width', 39);
        svg.documentElement.setAttribute('height', 39);
        svg.documentElement.setAttribute('preserveAspectRatio', 'xMidYMid meet');
        return xmlSerializer.serializeToString(svg);
    },

    /*
    requires

    \usepackage{tikz}
    \usetikzlibrary{svg.path}

    */
    make_qr_tex = function( url ) {
        const svg = new xmldom.DOMParser().parseFromString( qr.imageSync( url, { type: 'svg', ec_level: 'L'} ), 'text/xml' ),
        svgPath = svg.firstChild.firstChild.attributes[0].nodeValue;

        let cnt = '\\begin{center}\n\\fbox{\\begin{tikzpicture}[yscale=-1,fill=black]\n';
        cnt = cnt.concat( '\\fill svg[scale=3] {' );
        cnt = cnt.concat( svgPath );
        cnt = cnt.concat( '};' );
        cnt = cnt.concat( '\n\\end{tikzpicture}}\n\\end{center}' );
        return cnt;
    },

    get_qr_image = function( url, format ) {
        if (format === 'latex') {
            return pandoc.RawBlock( 'tex', make_qr_tex( url ) );
        } else {
            return pandoc.RawBlock( 'html', make_qr_svg( url ) );
        }
    },

    get_iso_date = function( d ) {
        return (new Date( d )).toISOString();
    },

    get_commit_url = function( commit ) {
        return 'http://bitbucket.org/roed/nigdy-wiecej/commits/'.concat(commit);
    },

    make_commit_url = function(commit, timestamp) {
        const ts = get_iso_date(timestamp);
        return pandoc.Link(
            ['',['nobold'],[] ],
            [ pandoc.Str(ts) ],
            [ get_commit_url(commit), ts ]
        );
    },

    action = function(type,value,format,meta) {
        if (type === 'Div' && utils.is_in_class( 'commithash', value[0] ) ) {
            const res = pandoc.Div( ['',['commitqr'],[]], [ get_qr_image( get_commit_url(meta.commithash.c), format ) ] );
            if (format === 'latex') {
                return res;
            } else {
                return pandoc.Div( ['',['commiturl'],[]], [ res ] );
            }
        } else if (type === 'Span' && utils.is_in_class( 'commitdate', value[0] ) ) {
            return pandoc.Span( utils.remove_class( 'commitdate', value[0] ), [ make_commit_url(meta.commithash.c, meta.commitdate.c)  ] );
        } else if (format === 'latex') {
            if (type === 'Div' && utils.is_in_class( 'commitinfo', value[0] )) {
                return pandoc.Div( utils.remove_class( 'commitinfo', value[0] ),
                    utils.wrap_with_tex_code( value[1], '\\vfill ', '' )
                );
            } else if (type === 'Div' && utils.is_in_class( 'committ', value[0] )) {
                return pandoc.Div( utils.remove_class( 'committ', value[0] ),
                    utils.wrap_with_tex_code( value[1], '\\begin{flushright}{\\tiny {\\tt ', '}}\\end{flushright}' )
                );
            }
        }
    };

pandoc.stdio(action);
