#!/bin/node

const
    pandoc = require('pandoc-filter'),
    utils = require('./pandoc-filter-utils'),
    xmldom = require('xmldom'),
    fa = require('../fontawesome/icons.json'),

    make_fa_svg = function( iconInfo, style ) {
	const
          rawSvg = iconInfo["svg"][style]["raw"],
          domParser = new xmldom.DOMParser(),
	  svg = domParser.parseFromString( rawSvg, 'text/xml' ),
          xmlSerializer = new xmldom.XMLSerializer();
	svg.documentElement.setAttribute('class', 'fa');
        svg.documentElement.setAttribute('width', iconInfo["svg"][style]["width"]);
        svg.documentElement.setAttribute('height', iconInfo["svg"][style]["height"]);
        svg.documentElement.setAttribute('preserveAspectRatio', 'xMidYMid meet');
        return xmlSerializer.serializeToString(svg);
    },

    /*
    requires

    \usepackage{tikz}
    \usetikzlibrary{svg.path}

    */
    make_fa_tex = function( iconInfo, style ) {
        let cnt = '\\resizebox{0.9em}{!}{\\begin{tikzpicture}[yscale=-1,fill=black]\n';
        cnt = cnt.concat( '\\fill svg {' );
        cnt = cnt.concat( iconInfo["svg"][style]["path"] );
        cnt = cnt.concat( '};' );
        cnt = cnt.concat( '\n\\end{tikzpicture}}' );
        return cnt;
    },

    get_fa_image = function( iconInfo, format ) {
	const style = iconInfo["styles"][0];
        if (format === 'latex') {
            const res = pandoc.RawInline( 'tex', make_fa_tex( iconInfo, style ) );
            // console.error(res);
            return res;
        } else {
            const res = pandoc.RawInline( 'html', make_fa_svg( iconInfo, style ) );
            // console.error(res);
            return res;
        }
    },

    action = function(type,value,format,meta) {
        if (type === 'Span' && utils.is_in_class( 'fa', value[0] ) ) {
		const faName = value[1][0].c;
		if (fa.hasOwnProperty(faName)) {
			return get_fa_image(fa[faName], format);
		}
        }
    };

pandoc.stdio(action);
